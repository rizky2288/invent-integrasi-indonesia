package com.rizky.testinginventintegrasi.feature.main.profile;

import com.rizky.testinginventintegrasi.R;
import com.rizky.testinginventintegrasi.component.BasePresenter;
import com.rizky.testinginventintegrasi.component.BaseView;
import com.rizky.testinginventintegrasi.data.SharedPref;

public class ProfilePresenter extends BasePresenter<ProfileView> {
    public void getUserData() {
        String name = session.getUserDetails().get(SharedPref.KEY_CUSTOMER_NAME);
        String phone = session.getUserDetails().get(SharedPref.KEY_CUSTOMER_PHONE);
        String email = session.getUserDetails().get(SharedPref.KEY_CUSTOMER_EMAIL);

        ifViewAttached(view -> view.setUserData(name, phone, email));
    }

    public void prepareLogout() {
        ifViewAttached(view -> view.showLoading(activity.getString(R.string.please_wait)));
        session.logoutUser();
        ifViewAttached(BaseView::hideLoading);
    }
}
