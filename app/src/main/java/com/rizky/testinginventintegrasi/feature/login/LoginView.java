package com.rizky.testinginventintegrasi.feature.login;

import com.rizky.testinginventintegrasi.component.BaseView;

public interface LoginView extends BaseView {
    void showAlertBlocked();

    void pindahHome();
}
