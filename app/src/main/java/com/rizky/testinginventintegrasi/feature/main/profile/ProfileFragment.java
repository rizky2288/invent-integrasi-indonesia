package com.rizky.testinginventintegrasi.feature.main.profile;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rizky.testinginventintegrasi.R;
import com.rizky.testinginventintegrasi.component.BaseFragment;
import com.rizky.testinginventintegrasi.databinding.FragmentProfileBinding;

public class ProfileFragment extends BaseFragment<ProfileView, ProfilePresenter> implements ProfileView, View.OnClickListener {
    private FragmentProfileBinding binding;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getUserData();

        setListener();
    }

    private void setListener() {
        binding.btnLogout.setOnClickListener(this);
    }

    @NonNull
    @Override
    public ProfilePresenter createPresenter() {
        return new ProfilePresenter();
    }

    @Override
    public void setUserData(String name, String phone, String email) {
        binding.edtName.setText(name);
        binding.edtPhone.setText(phone);
        binding.edtEmail.setText(email);
        binding.edtName.setEnabled(false);
        binding.edtPhone.setEnabled(false);
        binding.edtEmail.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < durationBetweenClick)
            return;
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.btn_logout:
                presenter.prepareLogout();
                break;
        }
    }
}