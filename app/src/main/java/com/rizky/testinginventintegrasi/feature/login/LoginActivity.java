package com.rizky.testinginventintegrasi.feature.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rizky.testinginventintegrasi.BaseMethodHelper;
import com.rizky.testinginventintegrasi.R;
import com.rizky.testinginventintegrasi.component.BaseActivity;
import com.rizky.testinginventintegrasi.databinding.ActivityLoginBinding;
import com.rizky.testinginventintegrasi.feature.main.MainActivity;
import com.rizky.testinginventintegrasi.feature.registrasi.RegistrasiActivity;

public class LoginActivity extends BaseActivity<LoginView, LoginPresenter> implements LoginView, View.OnClickListener {
    private ActivityLoginBinding binding;
    private static final int REQ_REGISTRASI = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        presenter.checkLoginSession();
        setListener();
    }

    private void setListener() {
        binding.txtRegistrasi.setOnClickListener(this);
        binding.btnLogin.setOnClickListener(this);
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < durationBetweenClick)
            return;
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.txt_registrasi:
                intentRegistrasi();
                break;
            case R.id.btn_login:
                prepareLogin();
                break;
        }
    }

    private void prepareLogin() {
        showLoading(getString(R.string.please_wait));
        resetErrorMessage();
        boolean isValid = true;
        String email = binding.edtEmail.getText().toString();
        String password = binding.edtPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            isValid = false;
            binding.txtUsername.setError(getString(R.string.error_text_empty, binding.edtEmail.getHint()));
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                isValid = false;
                binding.txtUsername.setError(getString(R.string.msg_error_format_email));
            }
        }

        if (TextUtils.isEmpty(password)) {
            isValid = false;
            binding.txtPassword.setError(getString(R.string.error_text_empty, binding.edtPassword.getHint()));
        }

        if (isValid) {
            presenter.loginUser(email, password);
        } else {
            hideLoading();
        }
    }

    private void resetErrorMessage() {
        binding.txtUsername.setError(null);
        binding.txtPassword.setError(null);
    }

    private void intentRegistrasi() {
        Intent intent = new Intent(this, RegistrasiActivity.class);
        startActivityForResult(intent, REQ_REGISTRASI);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQ_REGISTRASI) {
                showAlertBlocked();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showAlertBlocked() {
        BaseMethodHelper.showAlert(this, "Periksa email anda untuk Verifikasi email");
    }

    @Override
    public void pindahHome() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}