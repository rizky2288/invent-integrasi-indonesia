package com.rizky.testinginventintegrasi.feature.registrasi;

import android.app.Activity;

import com.rizky.testinginventintegrasi.BaseMethodHelper;
import com.rizky.testinginventintegrasi.component.BasePresenter;
import com.rizky.testinginventintegrasi.component.BaseView;
import com.rizky.testinginventintegrasi.data.api.body.RegisterCustomerBody;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;
import com.rizky.testinginventintegrasi.data.datasource.AuthRemoteDataSource;

import retrofit2.Response;

public class RegistrasiPresenter extends BasePresenter<RegistrasiView> {

    public void registUser(String name, String phone, String email, String password) {
        AuthRemoteDataSource dataSource = AuthRemoteDataSource.getInstance(apiStores);
        RegisterCustomerBody body = new RegisterCustomerBody(name, phone, email, password);
        dataSource.registUser(body, new AuthRemoteDataSource.AuthRemoteDataSourceCallback.RegisterCustomerCallback() {
            @Override
            public void onDataAvailable() {
                ifViewAttached(BaseView::hideLoading);
                activity.setResult(Activity.RESULT_OK);
                ifViewAttached(RegistrasiView::finsih);
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity,"Registrasi User", response, t);
            }
        });
    }
}
