package com.rizky.testinginventintegrasi.feature.main;

import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.android.material.tabs.TabLayoutMediator;
import com.rizky.testinginventintegrasi.adapter.ViewPagerAdapter;
import com.rizky.testinginventintegrasi.component.BaseActivity;
import com.rizky.testinginventintegrasi.databinding.ActivityMainBinding;
import com.rizky.testinginventintegrasi.feature.main.home.HomeFragment;
import com.rizky.testinginventintegrasi.feature.main.profile.ProfileFragment;

public class MainActivity extends BaseActivity<MainView, MainPresenter> implements MainView {
    private ActivityMainBinding binding;
    private ViewPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setPager();
    }

    private void setPager() {
        adapter = new ViewPagerAdapter(this);
        adapter.addFragment(new HomeFragment(), "Home");
        adapter.addFragment(new ProfileFragment(), "Profile");
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setOffscreenPageLimit(2);
        new TabLayoutMediator(binding.tabLayout, binding.viewPager,
                (tab, position) -> tab.setText(adapter.getPageTitle(position))
        ).attach();
    }

    @NonNull
    @Override
    public MainPresenter createPresenter() {
        return new MainPresenter();
    }
}