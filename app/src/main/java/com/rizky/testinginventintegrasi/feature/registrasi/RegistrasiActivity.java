package com.rizky.testinginventintegrasi.feature.registrasi;

import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import androidx.annotation.NonNull;

import com.rizky.testinginventintegrasi.BaseMethodHelper;
import com.rizky.testinginventintegrasi.R;
import com.rizky.testinginventintegrasi.component.BaseActivity;
import com.rizky.testinginventintegrasi.databinding.ActivityRegistrasiBinding;

public class RegistrasiActivity extends BaseActivity<RegistrasiView, RegistrasiPresenter> implements RegistrasiView, View.OnClickListener {
    private ActivityRegistrasiBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegistrasiBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setListener();
    }

    private void setListener() {
        binding.btnRegis.setOnClickListener(this);
    }

    @NonNull
    @Override
    public RegistrasiPresenter createPresenter() {
        return new RegistrasiPresenter();
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < durationBetweenClick)
            return;
        mLastClickTime = SystemClock.elapsedRealtime();
        switch (v.getId()) {
            case R.id.btn_regis:
                prepareRegistrasi();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    private void prepareRegistrasi() {
        showLoading(getString(R.string.please_wait));
        resetErrorMessage();
        boolean isValid = true;

        String name = binding.edtName.getText().toString();
        String phone = binding.edtPhone.getText().toString();
        String email = binding.edtEmail.getText().toString();
        String password = binding.edtPassword.getText().toString();

        if (TextUtils.isEmpty(name)) {
            isValid = false;
            binding.textInputLayoutName.setError(getString(R.string.error_text_empty, binding.edtName.getHint()));
        }

        if (TextUtils.isEmpty(phone)) {
            isValid = false;
            binding.textInputLayoutPhone.setError(getString(R.string.error_text_empty, binding.edtPhone.getHint()));
        } else {
            if (phone.length() < 7 || phone.length() > 15) {
                isValid = false;
                binding.textInputLayoutPhone.setError(getString(R.string.msg_error_format_phone));
            }
        }

        if (TextUtils.isEmpty(email)) {
            isValid = false;
            binding.textInputLayoutEmail.setError(getString(R.string.error_text_empty, binding.edtEmail.getHint()));
        } else {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                isValid = false;
                binding.textInputLayoutEmail.setError(getString(R.string.msg_error_format_email));
            }
        }

        if (TextUtils.isEmpty(password)) {
            isValid = false;
            binding.textInputLayoutPassword.setError(getString(R.string.error_text_empty, binding.edtPassword.getHint()));
        }

        if (isValid) {
            //regist
            if (presenter != null) {
                presenter.registUser(name,
                        phone,
                        email,
                        password);
            } else {
                BaseMethodHelper.showAlert(this, "presenter null");
                hideLoading();
            }
        } else {
            hideLoading();
        }
    }

    private void resetErrorMessage() {
        binding.textInputLayoutName.setError(null);
        binding.textInputLayoutPhone.setError(null);
        binding.textInputLayoutEmail.setError(null);
        binding.textInputLayoutPassword.setError(null);
    }

    @Override
    public void finsih() {
        finish();
    }
}