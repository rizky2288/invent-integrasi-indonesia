package com.rizky.testinginventintegrasi.feature.login;

import android.text.TextUtils;

import com.rizky.testinginventintegrasi.BaseMethodHelper;
import com.rizky.testinginventintegrasi.component.BasePresenter;
import com.rizky.testinginventintegrasi.component.BaseView;
import com.rizky.testinginventintegrasi.data.SharedPref;
import com.rizky.testinginventintegrasi.data.api.body.LoginUserBody;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;
import com.rizky.testinginventintegrasi.data.datasource.AuthRemoteDataSource;
import com.rizky.testinginventintegrasi.model.User;

import retrofit2.Response;

public class LoginPresenter extends BasePresenter<LoginView> {
    public void loginUser(String email, String password) {
        AuthRemoteDataSource dataSource = AuthRemoteDataSource.getInstance(apiStores);
        LoginUserBody body = new LoginUserBody(email, password);
        dataSource.loginCustomer(body, new AuthRemoteDataSource.AuthRemoteDataSourceCallback.LoginCustomerCallback() {
            @Override
            public void onDataAvailable(User user) {
                session.modifyLoginSession(SharedPref.KEY_CUSTOMER_ID, user.getCustomer_id());
                session.modifyLoginSession(SharedPref.KEY_CUSTOMER_NAME, user.getCustomer_name());
                session.modifyLoginSession(SharedPref.KEY_CUSTOMER_PHONE, user.getPhone());
                session.modifyLoginSession(SharedPref.KEY_CUSTOMER_EMAIL, user.getEmail());
                Boolean isBlocked = Boolean.valueOf(user.getBlocked());
                if (isBlocked) {
                    ifViewAttached(LoginView::showAlertBlocked);
                } else {
                    ifViewAttached(LoginView::pindahHome);
                }
                ifViewAttached(BaseView::hideLoading);
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse<User>> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity, "Login", response, t);
            }
        });
    }

    public void checkLoginSession() {
        if (!TextUtils.isEmpty(session.getUserDetails().get(SharedPref.KEY_CUSTOMER_ID))) {
            ifViewAttached(LoginView::pindahHome);
        }
    }
}
