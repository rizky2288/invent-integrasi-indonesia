package com.rizky.testinginventintegrasi.feature.main.home;

import com.rizky.testinginventintegrasi.component.BaseView;
import com.rizky.testinginventintegrasi.model.Kecamatan;
import com.rizky.testinginventintegrasi.model.Kelurahan;
import com.rizky.testinginventintegrasi.model.KodePos;
import com.rizky.testinginventintegrasi.model.Kota;
import com.rizky.testinginventintegrasi.model.Provinsi;

import java.util.List;

public interface HomeView extends BaseView {
    void setProvinsi(List<Provinsi> provinsiList);

    void setKota(List<Kota> kotaList);

    void setKelurahan(List<Kelurahan> kelurahanList);

    void setKecamatan(List<Kecamatan> kecamatanList);

    void setKodePos(List<KodePos> kodePosList);
}
