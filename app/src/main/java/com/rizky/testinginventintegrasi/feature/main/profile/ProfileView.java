package com.rizky.testinginventintegrasi.feature.main.profile;

import com.rizky.testinginventintegrasi.component.BaseView;

public interface ProfileView extends BaseView {
    void setUserData(String name, String phone, String email);
}
