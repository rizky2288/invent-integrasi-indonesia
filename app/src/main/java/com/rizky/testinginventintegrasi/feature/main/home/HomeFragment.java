package com.rizky.testinginventintegrasi.feature.main.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rizky.testinginventintegrasi.BaseMethodHelper;
import com.rizky.testinginventintegrasi.R;
import com.rizky.testinginventintegrasi.component.BaseFragment;
import com.rizky.testinginventintegrasi.databinding.FragmentHomeBinding;
import com.rizky.testinginventintegrasi.model.Kecamatan;
import com.rizky.testinginventintegrasi.model.Kelurahan;
import com.rizky.testinginventintegrasi.model.KodePos;
import com.rizky.testinginventintegrasi.model.Kota;
import com.rizky.testinginventintegrasi.model.Provinsi;

import java.util.List;

public class HomeFragment extends BaseFragment<HomeView, HomePresenter> implements HomeView {
    private FragmentHomeBinding binding;

    public HomeFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getProvinsi();
    }

    @NonNull
    @Override
    public HomePresenter createPresenter() {
        return new HomePresenter();
    }

    @Override
    public void setProvinsi(List<Provinsi> provinsiList) {
        binding.provinsi.setAdapter(BaseMethodHelper.setSpinner(getContext(), provinsiList, "region_name", R.string.toggle_choose_province));
        binding.provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    binding.kota.setVisibility(View.GONE);
                } else {
                    presenter.getKota(provinsiList.get(position-1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setKota(List<Kota> kotaList) {
        binding.kota.setAdapter(BaseMethodHelper.setSpinner(getContext(), kotaList, "district_name", R.string.toggle_choose_kota));
        binding.kota.setVisibility(View.VISIBLE);
        binding.kota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    binding.kelurahan.setVisibility(View.GONE);
                } else {
                    presenter.getKelurahan(kotaList.get(position-1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setKelurahan(List<Kelurahan> kelurahanList) {
        binding.kelurahan.setAdapter(BaseMethodHelper.setSpinner(getContext(), kelurahanList, "area_name", R.string.toggle_choose_kelurahan));
        binding.kelurahan.setVisibility(View.VISIBLE);
        binding.kelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    binding.kecamatan.setVisibility(View.GONE);
                } else {
                    presenter.getKecamatan(kelurahanList.get(position-1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setKecamatan(List<Kecamatan> kecamatanList) {
        binding.kecamatan.setAdapter(BaseMethodHelper.setSpinner(getContext(), kecamatanList, "branch_name", R.string.toggle_choose_kecamatan));
        binding.kecamatan.setVisibility(View.VISIBLE);
        binding.kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    binding.kodepos.setVisibility(View.GONE);
                } else {
                    presenter.getKodePos(kecamatanList.get(position-1));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setKodePos(List<KodePos> kodePosList) {
        binding.kodepos.setAdapter(BaseMethodHelper.setSpinner(getContext(), kodePosList, "post_code", R.string.toggle_choose_kodepos));
        binding.kodepos.setVisibility(View.VISIBLE);
    }
}