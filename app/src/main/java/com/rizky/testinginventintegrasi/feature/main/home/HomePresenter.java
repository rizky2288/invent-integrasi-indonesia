package com.rizky.testinginventintegrasi.feature.main.home;

import com.rizky.testinginventintegrasi.BaseMethodHelper;
import com.rizky.testinginventintegrasi.R;
import com.rizky.testinginventintegrasi.component.BasePresenter;
import com.rizky.testinginventintegrasi.component.BaseView;
import com.rizky.testinginventintegrasi.data.api.body.GetKecamatanBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKelurahanBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKodePosBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKotaBody;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;
import com.rizky.testinginventintegrasi.data.datasource.RegionDataSource;
import com.rizky.testinginventintegrasi.model.Kecamatan;
import com.rizky.testinginventintegrasi.model.Kelurahan;
import com.rizky.testinginventintegrasi.model.KodePos;
import com.rizky.testinginventintegrasi.model.Kota;
import com.rizky.testinginventintegrasi.model.Provinsi;

import java.util.List;

import retrofit2.Response;

public class HomePresenter extends BasePresenter<HomeView> {

    public void getProvinsi() {
        ifViewAttached(view -> view.showLoading(activity.getString(R.string.please_wait)));
        RegionDataSource dataSource = RegionDataSource.getInstance(apiStores);
        dataSource.getProvinsi(new Object(), new RegionDataSource.RegionDataSourceCallback.GetProvinsiCallback() {
            @Override
            public void onDataAvailable(List<Provinsi> provinsiList) {
                ifViewAttached(view -> {
                    view.setProvinsi(provinsiList);
                    view.hideLoading();
                });
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse<List<Provinsi>>> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity, "getProvinsi", response, t);
            }
        });
    }

    public void getKota(Provinsi provinsi) {
        ifViewAttached(view -> view.showLoading(activity.getString(R.string.please_wait)));
        RegionDataSource dataSource = RegionDataSource.getInstance(apiStores);
        GetKotaBody body = new GetKotaBody(provinsi.getRegion_id());
        dataSource.getKota(body, new RegionDataSource.RegionDataSourceCallback.GetKotaCallback() {
            @Override
            public void onDataAvailable(List<Kota> kotaList) {
                ifViewAttached(view -> {
                    view.setKota(kotaList);
                    view.hideLoading();
                });
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse<List<Kota>>> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity, "GetKota", response, t);
            }
        });
    }

    public void getKecamatan(Kelurahan kelurahan) {
        ifViewAttached(view -> view.showLoading(activity.getString(R.string.please_wait)));
        GetKecamatanBody body = new GetKecamatanBody();
        body.setArea_id(kelurahan.getArea_id());
        RegionDataSource dataSource = RegionDataSource.getInstance(apiStores);
        dataSource.getKecamatan(body, new RegionDataSource.RegionDataSourceCallback.GetKecamatanCallback() {
            @Override
            public void onDataAvailable(List<Kecamatan> kecamatanList) {
                ifViewAttached(view -> {
                    view.setKecamatan(kecamatanList);
                    view.hideLoading();
                });
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse<List<Kecamatan>>> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity, "GetKecamatan", response, t);
            }
        });
    }

    public void getKelurahan(Kota kota) {
        ifViewAttached(view -> view.showLoading(activity.getString(R.string.please_wait)));
        GetKelurahanBody body = new GetKelurahanBody();
        body.setDistrict_id(kota.getDistrict_id());
        RegionDataSource dataSource = RegionDataSource.getInstance(apiStores);
        dataSource.getKelurahan(body, new RegionDataSource.RegionDataSourceCallback.GetKelurahanCallback() {
            @Override
            public void onDataAvailable(List<Kelurahan> kelurahanList) {
                ifViewAttached(view -> {
                    view.setKelurahan(kelurahanList);
                    view.hideLoading();
                });
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse<List<Kelurahan>>> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity, "GetKelurahan", response, t);
            }
        });
    }

    public void getKodePos(Kecamatan kecamatan) {
        ifViewAttached(view -> view.showLoading(activity.getString(R.string.please_wait)));
        GetKodePosBody body = new GetKodePosBody();
        body.setBranch_id(kecamatan.getBranch_id());
        RegionDataSource dataSource = RegionDataSource.getInstance(apiStores);
        dataSource.getKodePos(body, new RegionDataSource.RegionDataSourceCallback.GetKodePosCallback() {
            @Override
            public void onDataAvailable(List<KodePos> kodePosList) {
                ifViewAttached(view -> {
                    view.setKodePos(kodePosList);
                    view.hideLoading();
                });
            }

            @Override
            public void onDataNotAvailable(Response<BaseResponse<List<KodePos>>> response, Throwable t) {
                ifViewAttached(BaseView::hideLoading);
                BaseMethodHelper.handleErrorApi(activity, "GetKodePos", response, t);
            }
        });
    }
}
