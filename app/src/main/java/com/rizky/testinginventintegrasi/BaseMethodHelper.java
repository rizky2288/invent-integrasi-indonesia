package com.rizky.testinginventintegrasi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import javax.net.ssl.SSLPeerUnverifiedException;

import retrofit2.Response;

public class BaseMethodHelper {
    public static void handleErrorApi(Activity activity, String module, Response response, Throwable t) {
        Response<BaseResponse> baseResponse = (Response<BaseResponse>) response;
        if (baseResponse == null) {
            if (t == null) {
                showAlertwiththread(activity, activity.getString(R.string.error_no_internet_connection));
            } else {
                if (t instanceof SocketTimeoutException) {
                    showAlertwiththread(activity, activity.getString(R.string.error_timed_out));
                } else if (t instanceof UnknownHostException) {
                    showAlertwiththread(activity, activity.getString(R.string.error_no_internet_connection));
                } else if (t instanceof SSLPeerUnverifiedException) {
                    showAlertwiththread(activity, activity.getString(R.string.certificate_pin_failure));
                } else {
                    showErrorAlertwiththread(activity, module, t.getMessage());
                }
            }
        } else {
            if (baseResponse.body() == null) {
                showErrorAlertwiththread(activity, module, response.message());
            } else {
                showAlert(activity, baseResponse.body().getStatus_message_ind());
            }
        }
    }

    public static void showErrorAlertwiththread(final Activity activity, final String module, final String error) {
        activity.runOnUiThread(() -> showErrorAlert(activity, module, error));
    }

    public static void showErrorAlert(final Activity activity, final String posisiError, final String error) {
        new AlertDialog.Builder(activity)
                .setMessage(R.string.message_error_application)
                .setCancelable(false)
                .setNeutralButton(R.string.detail_error_application, (dialogInterface, i) -> new AlertDialog.Builder(activity)
                        .setMessage(activity.getLocalClassName() + " || " + posisiError + " || " + "ERROR: " + error)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, null)
                        .show())
                .setPositiveButton(R.string.text_close_button, null)
                .show();

    }

    private static void showAlertwiththread(Activity activity, String message) {
        activity.runOnUiThread(() -> showAlert(activity, message));
    }

    public static void showAlert(Activity activity, String message) {
        new AlertDialog.Builder(activity)
                .setMessage("" + message)
                .setCancelable(false)
                .setPositiveButton(R.string.text_close_button, null)
                .show();
    }

    public static ArrayAdapter<String> setSpinner(Context context, List<?> listObject, String param, int stringResId) {
        //populating items[] from List Object based on choice
        Gson gson = new Gson();
        String[] items;
        boolean isError = false;
        if (stringResId != 0) {
            items = new String[listObject.size() + 1];
            items[0] = context.getString(stringResId);

            for (int i = 0; i < listObject.size(); i++) {
                try {
                    items[i + 1] = new JSONObject(gson.toJson(listObject.get(i))).getString(param);
                } catch (JSONException e) {
                    isError = true;
                    e.printStackTrace();
                }
            }
        } else {
            items = new String[listObject.size()];

            for (int i = 0; i < listObject.size(); i++) {
                try {
                    items[i] = new JSONObject(gson.toJson(listObject.get(i))).getString(param);
                } catch (JSONException e) {
                    isError = true;
                    e.printStackTrace();
                }
            }
        }

        if (isError) {
            if (stringResId != 0) {
                items = new String[listObject.size() + 1];
                items[0] = context.getString(stringResId);
            } else {
                items = new String[listObject.size()];
            }
            for (int i = 0; i < listObject.size(); i++) {
                boolean isString = false;
                String s = listObject.get(i).toString();
                //find first index
                int firstIndex = s.indexOf(param + "=") + param.length() + 1;
                char c = s.charAt(firstIndex);
                if (c == '\'') {
                    firstIndex = firstIndex + 1;
                    isString = true;
                }
                //find last index, find comma from first index position
                int lastIndex = s.indexOf(",", firstIndex);
                //if no comma found, means eof
                if (lastIndex == -1) lastIndex = s.length() - 1;
                //if string is true means there is ' on end
                if (isString) lastIndex = lastIndex - 1;

                String sub = s.substring(firstIndex, lastIndex);
                if (stringResId != 0) {
                    items[i + 1] = sub;
                } else {
                    items[i] = sub;
                }
            }
        }
        return spinnerAdapter(context, items);
    }

    public static ArrayAdapter<String> spinnerAdapter(Context context, String[] items) {
        ArrayAdapter<String> adapter;
        adapter = new ArrayAdapter<String>(context, R.layout.spinner_item, items) {
            @NonNull
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                return super.getView(position, convertView, parent);
            }

            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                return super.getDropDownView(position, convertView, parent);
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapter;
    }
}
