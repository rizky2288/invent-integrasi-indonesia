package com.rizky.testinginventintegrasi.component;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;

/**
 * Base Fragment inherited from MVPFragment
 */
public abstract class BaseFragment<V extends BaseView, P extends BasePresenter<V>> extends MvpFragment
        implements BaseView{

    protected P presenter;
    protected ProgressDialog progressDialog = null;
    protected long mLastClickTime = 0;
    protected long durationBetweenClick = 1000;

    /**
     * Initializing fragment and binding the layout
     * @param view View
     * @param savedInstanceState Saved Instance State from previous state, maybe null
     */
    @CallSuper
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        presenter=(P)getPresenter();
        presenter.initiatePresenter(getActivity(),this,getArguments());
    }

    /**
     * {@inheritDoc}
     */
    @NonNull
    @Override public abstract P createPresenter();

    /**
     * {@inheritDoc}
     */
    @CallSuper
    @Override
    public void showLoading(String message) {
        if (progressDialog==null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
        } else {
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    /**
     * {@inheritDoc}
     */
    @CallSuper
    @Override
    public void hideLoading() {
        if (progressDialog!=null)
            progressDialog.dismiss();
    }

}
