package com.rizky.testinginventintegrasi.component;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Base View inherited from MVPView
 */
public interface BaseView extends MvpView {
    void showLoading(String message);

    void hideLoading();
}
