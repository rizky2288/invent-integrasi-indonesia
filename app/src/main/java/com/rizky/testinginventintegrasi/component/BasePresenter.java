package com.rizky.testinginventintegrasi.component;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.CallSuper;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.rizky.testinginventintegrasi.data.SharedPref;
import com.rizky.testinginventintegrasi.data.api.ApiStores;
import com.rizky.testinginventintegrasi.data.api.NetworkClient;

/**
 * Base Presenter inherited from MVPBasePresenter
 */
public class BasePresenter<V extends BaseView> extends MvpBasePresenter<V> {
    protected Activity activity;
    protected Fragment fragment;
    protected Bundle arguments;
    protected SharedPref session;
    protected ApiStores apiStores;
    protected Intent intent;
    protected Gson gson;

    /**
     * Initiating Presenter from Activity
     *
     * @param intent   intent of the calling activity
     * @param activity calling activity
     */
    @CallSuper
    public void initiatePresenter(Intent intent, Activity activity) {
        this.activity = activity;
        this.intent = intent;
        session = new SharedPref(activity);
        gson = new Gson();
        apiStores = NetworkClient.getRetrofit(activity).create(ApiStores.class);
    }

    /**
     * Initiating Presenter from Fragment
     *
     * @param activity  calling activity
     * @param fragment  calling fragment
     * @param arguments calling arguments of fragment
     */
    @CallSuper
    public void initiatePresenter(Activity activity, Fragment fragment, Bundle arguments) {
        this.activity = activity;
        this.fragment = fragment;
        this.arguments = arguments;
        session = new SharedPref(activity);
        gson = new Gson();
        apiStores = NetworkClient.getRetrofit(activity).create(ApiStores.class);
    }
}
