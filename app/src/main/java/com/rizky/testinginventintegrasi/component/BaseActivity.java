package com.rizky.testinginventintegrasi.component;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;

/**
 * Base Activity inherited from MVPActivity
 */
public abstract class BaseActivity<V extends BaseView, P extends BasePresenter<V>> extends MvpActivity implements BaseView {
    protected P presenter;
    protected ProgressDialog progressDialog = null;
    protected long mLastClickTime = 0;
    protected long durationBetweenClick = 1000;

    /**
     * Initializing activity and binding the layout
     * @param savedInstanceState Saved Instance State from previous state, maybe null
     */
    @CallSuper
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = (P) getPresenter();
        presenter.initiatePresenter(getIntent(), this);
    }

    /**
     * Creating presenter for MVP Activity
     * @return Related Presenter object
     */
    @NonNull
    @Override
    public abstract P createPresenter();

    /**
     * {@inheritDoc}
     */
    @CallSuper
    @Override
    public void showLoading(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
        } else {
            progressDialog.setMessage(message);
        }
        progressDialog.show();
    }

    /**
     * {@inheritDoc}
     */
    @CallSuper
    @Override
    public void hideLoading() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
