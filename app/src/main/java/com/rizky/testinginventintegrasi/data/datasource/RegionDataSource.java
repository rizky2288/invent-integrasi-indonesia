package com.rizky.testinginventintegrasi.data.datasource;

import com.rizky.testinginventintegrasi.data.api.ApiStores;
import com.rizky.testinginventintegrasi.data.api.BaseCallback;
import com.rizky.testinginventintegrasi.data.api.body.GetKecamatanBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKelurahanBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKodePosBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKotaBody;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;
import com.rizky.testinginventintegrasi.model.Kecamatan;
import com.rizky.testinginventintegrasi.model.Kelurahan;
import com.rizky.testinginventintegrasi.model.KodePos;
import com.rizky.testinginventintegrasi.model.Kota;
import com.rizky.testinginventintegrasi.model.Provinsi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class RegionDataSource {
    private ApiStores mApiStores;

    public RegionDataSource(ApiStores mApiStores) {
        this.mApiStores = mApiStores;
    }

    public static RegionDataSource getInstance(ApiStores apiStores) {
        return new RegionDataSource(apiStores);
    }

    public void getProvinsi(Object body, RegionDataSourceCallback.GetProvinsiCallback callback) {
        mApiStores.getProvinsi(body)
                .enqueue(new BaseCallback<BaseResponse<List<Provinsi>>, Object>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Provinsi>>> call, Response<BaseResponse<List<Provinsi>>> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable(response.body().getValue());
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Provinsi>>> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }

    public void getKota(GetKotaBody body, RegionDataSourceCallback.GetKotaCallback callback) {
        mApiStores.getKota(body)
                .enqueue(new BaseCallback<BaseResponse<List<Kota>>, GetKotaBody>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Kota>>> call, Response<BaseResponse<List<Kota>>> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable(response.body().getValue());
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Kota>>> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }

    public void getKelurahan(GetKelurahanBody body, RegionDataSourceCallback.GetKelurahanCallback callback) {
        mApiStores.getKelurahan(body)
                .enqueue(new BaseCallback<BaseResponse<List<Kelurahan>>, GetKelurahanBody>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Kelurahan>>> call, Response<BaseResponse<List<Kelurahan>>> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable(response.body().getValue());
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Kelurahan>>> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }

    public void getKecamatan(GetKecamatanBody body, RegionDataSourceCallback.GetKecamatanCallback callback) {
        mApiStores.getKecamatan(body)
                .enqueue(new BaseCallback<BaseResponse<List<Kecamatan>>, GetKecamatanBody>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse<List<Kecamatan>>> call, Response<BaseResponse<List<Kecamatan>>> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable(response.body().getValue());
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<Kecamatan>>> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }

    public void getKodePos(GetKodePosBody body, RegionDataSourceCallback.GetKodePosCallback callback) {
        mApiStores.getKodePos(body)
                .enqueue(new BaseCallback<BaseResponse<List<KodePos>>, GetKodePosBody>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse<List<KodePos>>> call, Response<BaseResponse<List<KodePos>>> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable(response.body().getValue());
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<List<KodePos>>> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }

    public interface RegionDataSourceCallback {
        interface GetProvinsiCallback {
            void onDataAvailable(List<Provinsi> provinsiList);
            void onDataNotAvailable(Response<BaseResponse<List<Provinsi>>> response, Throwable t);
        }

        interface GetKotaCallback {
            void onDataAvailable(List<Kota> kotaList);
            void onDataNotAvailable(Response<BaseResponse<List<Kota>>> response, Throwable t);
        }

        interface GetKecamatanCallback {
            void onDataAvailable(List<Kecamatan> kecamatanList);
            void onDataNotAvailable(Response<BaseResponse<List<Kecamatan>>> response, Throwable t);
        }

        interface GetKelurahanCallback {
            void onDataAvailable(List<Kelurahan> kelurahanList);
            void onDataNotAvailable(Response<BaseResponse<List<Kelurahan>>> response, Throwable t);
        }

        interface GetKodePosCallback {
            void onDataAvailable(List<KodePos> kodePosList);
            void onDataNotAvailable(Response<BaseResponse<List<KodePos>>> response, Throwable t);
        }
    }
}
