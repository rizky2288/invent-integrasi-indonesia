package com.rizky.testinginventintegrasi.data.api;

import android.content.Context;
import android.text.TextUtils;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.rizky.testinginventintegrasi.data.SharedPref;

import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {
    private static Retrofit retrofit;
    private static String baseUrl = "https://invent-integrasi.com/brum_core/mobile/bypass/";
    private static OkHttpClient okHttpClient;

    public static void CancelAllRequest() {
        okHttpClient.dispatcher().cancelAll();
    }

    public static Retrofit getRetrofit(Context context) {
        SharedPref sharedPref = new SharedPref(context);
        // Create a trust manager that does not validate certificate chains
        try {
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {

                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {

                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                    .hostnameVerifier((hostname, session) -> true)
                    .addInterceptor(chain -> {
                        // Add interceptor for embedding access token and signature
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder();
                        requestBuilder.headers(original.headers());
                        if (!TextUtils.isEmpty(sharedPref.getUserDetails().get(SharedPref.KEY_CUSTOMER_ID))) {
                            requestBuilder.header("Authorization", "A");
                            requestBuilder.header("Key", "A");
                            requestBuilder.header("Timestamp", "2020-01-10 18:02:00");
                            requestBuilder.header("Signature", "A");
                        }
                        requestBuilder.method(original.method(), original.body());
                        return chain.proceed(requestBuilder.build());
                    })
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        if (okHttpClient == null) {
            okHttpClient = new OkHttpClient.Builder()
                    .readTimeout(10, TimeUnit.SECONDS)
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .addInterceptor(chain -> {
                        // Add interceptor for embedding access token and signature
                        Request original = chain.request();
                        Request.Builder requestBuilder = original.newBuilder();
                        requestBuilder.headers(original.headers());
                        if (!TextUtils.isEmpty(sharedPref.getUserDetails().get(SharedPref.KEY_CUSTOMER_ID))) {
                            requestBuilder.header("Authorization", "A");
                            requestBuilder.header("Key", "A");
                            requestBuilder.header("Timestamp", "2020-01-10 18:02:00");
                            requestBuilder.header("Signature", "A");
                        }
                        requestBuilder.method(original.method(), original.body());
                        return chain.proceed(requestBuilder.build());
                    })
                    .addNetworkInterceptor(new StethoInterceptor())
                    .build();
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
        return retrofit;
    }
}
