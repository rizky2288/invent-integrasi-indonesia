package com.rizky.testinginventintegrasi.data.api;

import androidx.annotation.CallSuper;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class BaseCallback<R,B> implements Callback<R> {
    private B body;
    private Gson gson;

    public BaseCallback(B body) {
        this.body = body;
        gson = new Gson();
    }

    @CallSuper
    @Override
    public void onResponse(Call<R> call, Response<R> response) {
        new Thread(() -> {
            // add logging here
        }).start();
    }

    @CallSuper
    @Override
    public void onFailure(Call<R> call, Throwable t) {
        new Thread(() -> {
            // add logging here
        }).start();
    }
}
