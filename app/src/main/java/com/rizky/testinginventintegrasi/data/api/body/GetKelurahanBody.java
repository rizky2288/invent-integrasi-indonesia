package com.rizky.testinginventintegrasi.data.api.body;

public class GetKelurahanBody {
    private String district_id;

    public String getDistrict_id() {
        return district_id;
    }

    public void setDistrict_id(String district_id) {
        this.district_id = district_id;
    }
}
