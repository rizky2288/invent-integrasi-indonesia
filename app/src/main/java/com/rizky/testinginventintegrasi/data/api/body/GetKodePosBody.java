package com.rizky.testinginventintegrasi.data.api.body;

public class GetKodePosBody {
    private String branch_id;

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }
}
