package com.rizky.testinginventintegrasi.data;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.rizky.testinginventintegrasi.feature.login.LoginActivity;

import java.util.HashMap;

public class SharedPref {
    private final SharedPreferences pref;
    private final SharedPreferences.Editor editor;
    private final Context _context;
    private static final String PREF_NAME = "RizkySharedPref";

    public static final String KEY_CUSTOMER_ID = "customer_id";
    public static final String KEY_CUSTOMER_NAME = "customer_name";
    public static final String KEY_CUSTOMER_PHONE = "customer_phone";
    public static final String KEY_CUSTOMER_EMAIL = "customer_email";

    @SuppressLint("CommitPrefEdits")
    public SharedPref(Context context) {
        this._context = context;
        int PRIVATE_MODE = 0;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void modifyLoginSession(String column, String value) {
        editor.putString(column, value);
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<>();
        user.put(KEY_CUSTOMER_ID, pref.getString(KEY_CUSTOMER_ID, null));
        user.put(KEY_CUSTOMER_NAME, pref.getString(KEY_CUSTOMER_NAME, null));
        user.put(KEY_CUSTOMER_PHONE, pref.getString(KEY_CUSTOMER_PHONE, null));
        user.put(KEY_CUSTOMER_EMAIL, pref.getString(KEY_CUSTOMER_EMAIL, null));
        return user;
    }

    public void logoutUser() {
        clearUserData();
        Intent i = new Intent(_context, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        _context.startActivity(i);
    }

    private void clearUserData() {
        editor.putString(KEY_CUSTOMER_ID, "");
        editor.putString(KEY_CUSTOMER_NAME, "");
        editor.putString(KEY_CUSTOMER_PHONE, "");
        editor.putString(KEY_CUSTOMER_EMAIL, "");
        editor.commit();
    }
}
