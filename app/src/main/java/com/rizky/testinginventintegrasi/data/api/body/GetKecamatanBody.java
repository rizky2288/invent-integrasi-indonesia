package com.rizky.testinginventintegrasi.data.api.body;

public class GetKecamatanBody {
    private String area_id;

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }
}
