package com.rizky.testinginventintegrasi.data.api.body;

public class RegisterCustomerBody {
    private String customer_name;
    private String phone;
    private String email;
    private String password;
    private String created_by;

    public RegisterCustomerBody(String customer_name, String phone, String email, String password) {
        this.customer_name = customer_name;
        this.phone = phone;
        this.email = email;
        this.password = password;
        created_by = "SYSTEM";
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
}
