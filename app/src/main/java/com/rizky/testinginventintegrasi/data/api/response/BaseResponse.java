package com.rizky.testinginventintegrasi.data.api.response;

import com.google.gson.annotations.SerializedName;

public class BaseResponse<V> {
    @SerializedName("status_code")
    private String status_code;

    @SerializedName("status_message_ind")
    private String status_message_ind;

    @SerializedName("status_message_eng")
    private String status_message_eng;

    @SerializedName("value")
    private V value;

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_message_ind() {
        return status_message_ind;
    }

    public void setStatus_message_ind(String status_message_ind) {
        this.status_message_ind = status_message_ind;
    }

    public String getStatus_message_eng() {
        return status_message_eng;
    }

    public void setStatus_message_eng(String status_message_eng) {
        this.status_message_eng = status_message_eng;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }
}
