package com.rizky.testinginventintegrasi.data.datasource;

import com.rizky.testinginventintegrasi.data.api.ApiStores;
import com.rizky.testinginventintegrasi.data.api.BaseCallback;
import com.rizky.testinginventintegrasi.data.api.body.LoginUserBody;
import com.rizky.testinginventintegrasi.data.api.body.RegisterCustomerBody;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;
import com.rizky.testinginventintegrasi.model.User;

import retrofit2.Call;
import retrofit2.Response;

public class AuthRemoteDataSource {
    private ApiStores mApiStores;

    public AuthRemoteDataSource(ApiStores mApiStores) {
        this.mApiStores = mApiStores;
    }

    public static AuthRemoteDataSource getInstance(ApiStores apiStores) {
        return new AuthRemoteDataSource(apiStores);
    }

    public void registUser(RegisterCustomerBody body, AuthRemoteDataSourceCallback.RegisterCustomerCallback callback) {
        mApiStores.registerCustomer(body)
                .enqueue(new BaseCallback<BaseResponse, RegisterCustomerBody>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable();
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }

    public void loginCustomer(LoginUserBody body, AuthRemoteDataSourceCallback.LoginCustomerCallback callback) {
        mApiStores.loginCustomer(body)
                .enqueue(new BaseCallback<BaseResponse<User>, LoginUserBody>(body) {
                    @Override
                    public void onResponse(Call<BaseResponse<User>> call, Response<BaseResponse<User>> response) {
                        super.onResponse(call, response);
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatus_code().equals("ERR-00-000")) {
                                    callback.onDataAvailable(response.body().getValue());
                                } else {
                                    callback.onDataNotAvailable(response, null);
                                }
                            } else {
                                callback.onDataNotAvailable(response, null);
                            }
                        } else {
                            callback.onDataNotAvailable(response, null);
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<User>> call, Throwable t) {
                        super.onFailure(call, t);
                        callback.onDataNotAvailable(null, t);
                    }
                });
    }


    public interface AuthRemoteDataSourceCallback {
        interface RegisterCustomerCallback {
            void onDataAvailable();
            void onDataNotAvailable(Response<BaseResponse> response, Throwable t);
        }

        interface LoginCustomerCallback {
            void onDataAvailable(User user);
            void onDataNotAvailable(Response<BaseResponse<User>> response, Throwable t);
        }
    }
}
