package com.rizky.testinginventintegrasi.data.api.body;

public class GetKotaBody {
    private String region_id;

    public GetKotaBody(String region_id) {
        this.region_id = region_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }
}
