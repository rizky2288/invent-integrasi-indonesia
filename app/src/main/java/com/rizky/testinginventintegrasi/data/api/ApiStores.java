package com.rizky.testinginventintegrasi.data.api;

import com.rizky.testinginventintegrasi.data.api.body.GetKecamatanBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKelurahanBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKodePosBody;
import com.rizky.testinginventintegrasi.data.api.body.GetKotaBody;
import com.rizky.testinginventintegrasi.data.api.body.LoginUserBody;
import com.rizky.testinginventintegrasi.data.api.body.RegisterCustomerBody;
import com.rizky.testinginventintegrasi.data.api.response.BaseResponse;
import com.rizky.testinginventintegrasi.model.Kecamatan;
import com.rizky.testinginventintegrasi.model.Kelurahan;
import com.rizky.testinginventintegrasi.model.KodePos;
import com.rizky.testinginventintegrasi.model.Kota;
import com.rizky.testinginventintegrasi.model.Provinsi;
import com.rizky.testinginventintegrasi.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiStores {
    @POST("reg-customer")
    Call<BaseResponse> registerCustomer(@Body RegisterCustomerBody body);

    @POST("login-customer")
    Call<BaseResponse<User>> loginCustomer(@Body LoginUserBody body);

    @POST("get-provinsi")
    Call<BaseResponse<List<Provinsi>>> getProvinsi(@Body Object body);

    @POST("get-kota")
    Call<BaseResponse<List<Kota>>> getKota(@Body GetKotaBody body);

    @POST("get-kecamatan")
    Call<BaseResponse<List<Kecamatan>>> getKecamatan(@Body GetKecamatanBody body);

    @POST("get-kelurahan")
    Call<BaseResponse<List<Kelurahan>>> getKelurahan(@Body GetKelurahanBody body);

    @POST("get-kodepos")
    Call<BaseResponse<List<KodePos>>> getKodePos(@Body GetKodePosBody body);
}
