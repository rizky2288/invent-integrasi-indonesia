package com.rizky.testinginventintegrasi.model;

import com.google.gson.annotations.SerializedName;

public class Kecamatan {
    @SerializedName("branch_id")
    private String branch_id;

    @SerializedName("branch_name")
    private String branch_name;

    public String getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(String branch_id) {
        this.branch_id = branch_id;
    }

    public String getBranch_name() {
        return branch_name;
    }

    public void setBranch_name(String branch_name) {
        this.branch_name = branch_name;
    }

    @Override
    public String toString() {
        return "Kecamatan{" +
                "branch_id='" + branch_id + '\'' +
                ", branch_name='" + branch_name + '\'' +
                '}';
    }
}
