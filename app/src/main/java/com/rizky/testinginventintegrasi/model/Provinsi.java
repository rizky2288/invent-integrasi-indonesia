package com.rizky.testinginventintegrasi.model;

import com.google.gson.annotations.SerializedName;

public class Provinsi {
    @SerializedName("region_id")
    private String region_id;

    @SerializedName("region_name")
    private String region_name;

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    @Override
    public String toString() {
        return "Provinsi{" +
                "region_id='" + region_id + '\'' +
                ", region_name='" + region_name + '\'' +
                '}';
    }
}
