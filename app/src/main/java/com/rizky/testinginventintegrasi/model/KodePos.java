package com.rizky.testinginventintegrasi.model;

import com.google.gson.annotations.SerializedName;

public class KodePos {
    @SerializedName("post_code")
    private String post_code;

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    @Override
    public String toString() {
        return "KodePos{" +
                "post_code='" + post_code + '\'' +
                '}';
    }
}
